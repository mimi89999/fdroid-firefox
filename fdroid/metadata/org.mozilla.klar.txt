License:MPL-2.0
Web Site:https://support.mozilla.org/products/klar
Issue Tracker:https://bugzilla.mozilla.org/
Donate:https://donate.mozilla.org/
Summary:Firefox Klar
Categories:Internet
Description:
Firefox Klar gives you a dedicated privacy browser with tracking protection and content blocking on your Android phone or tablet.

Klar by Firefox is the German-language version and only available in Germany, Austria, and Switzerland. It includes the same features as the English-language app Focus by Firefox. 
.
