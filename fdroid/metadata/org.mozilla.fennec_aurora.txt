License:MPL-2.0
Web Site:https://www.mozilla.org/firefox/android/nightly/all/
Issue Tracker:https://bugzilla.mozilla.org/
Donate:https://donate.mozilla.org/
Summary:Firefox Nightly
Categories:Internet
Description:
Nightly version of the Firefox browser for Android.
Please note that due to the update frequency of the nightly builds,
the apk offered here may be one day/version behind.
.
