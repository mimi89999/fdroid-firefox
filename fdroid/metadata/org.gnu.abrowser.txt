License:MPL-2.0
Source Code:https://github.com/proninyaroslav/abrowser-android/
Web Site:https://trisquel.info/en/wiki/abrowser-help
Issue Tracker:https://github.com/proninyaroslav/abrowser-android/issues
Donate:https://trisquel.info/en/donate
Summary:Abrowser
Categories:Internet
Description:
Abrowser is Trisquel's version of Mozilla's popular web browser with the trademarked logos replaced. Abrowser, other that saner default settings to improve privacy, is pretty much Firefox and should provide a quite "neutral" browsing experience.
.
